import Events from "events";
import type from "../../../constants/type";

const def = {
  BOARD: "BOARD",
  THREAD_TITLE: "THREAD_TITLE",
  THREAD_RES: "THREAD_RES",
  THREAD_ADDED: "THREAD_ADDED"
};

let connect, blockchainApp;

export default class Board {
  constructor(_connect, _blockchainApp) {
    connect = _connect;
    blockchainApp = _blockchainApp;

    this.ev = new Events.EventEmitter();
    this.threads = [];

    connect.ev.on("p2ch", netwokLayer => {
      const transportLayer = JSON.parse(netwokLayer);
      const sessionLayer = transportLayer.body;
      if (sessionLayer.type == type.APP) {
        const presenLayer = sessionLayer.data;
        const appLayer = presenLayer.data;

        switch (presenLayer.presen) {
          case def.THREAD_TITLE:
            this.onThread(appLayer);
            break;
        }
      }
    });
  }

  makeThread() {
    
  }

  onThread(data) {
    const title = data.title;
    if (!this.threads.toString().includes(title)) {
      this.threads.push(title);
      this.ev.emit(def.THREAD_ADDED, this.threads);
    }
  }
}
