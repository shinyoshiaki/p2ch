import React, { Component } from "react";
import { List, ListItem, ListItemText, Button, TextField } from "material-ui";
import { withRouter } from "react-router";
import Board from "./lib/board";
import type from '../../constants/type'
import * as format from "../../constants/format";

const def = {
  THREAD_ADDED: "THREAD_ADDED"
};

class BoardIndex extends Component {
  constructor(props) {
    super(props);
    this.state = { threads: [], threadTitle: "" };
    this.board = new Board(props.connect);

    this.board.ev.on(def.THREAD_ADDED, _threads => {
      this.setState({ threads: _threads });
    });
  }

  componentDidMount() {
    //this.setState({ threads: this.props.connect.threads })
  }

  render() {
    return (
      <div>
        <TextField
          onChange={e => this.setState({ threadTitle: e.target.value })}
        />
        <Button onClick={() => this.makeThread()}>make thread</Button>
        <br />
        <br />
        <List>
          {this.state.threads.length > 0 &&
            this.state.threads.forEach(title => {
              return (
                <ListItem
                  button
                  onClick={() =>
                    this.props.history.push({
                      pathname: "/thread",
                      state: { title: title }
                    })
                  }
                >
                  <ListItemText primary={title} />
                </ListItem>
              );
            })}
        </List>
      </div>
    );
  }
}

export default withRouter(BoardIndex);
