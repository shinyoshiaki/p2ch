import React, { Component } from "react";
import { Button, TextField } from "material-ui";
import * as format from "../constants/format";
import type from "../constants/type";
import Token from "../blockchain/token";

let token;
let blockchain;
let nodeId;

export default class BlockchainComponent extends Component {
  constructor(props) {
    super(props);

    blockchain = props.blockchainApp.blockchain;
    token = new Token(props.connect.userId, blockchain);
    nodeId = props.connect.userId;

    this.state = {
      tokenNum: token.nowAmount(),
      targetId: "",
      tokenAmount: 0
    };

    this.props.connect.ev.on("p2ch", networkLayer => {
      this.setState({ tokenNum: token.nowAmount() });
    });
  }

  componentDidMount() {
    console.log("oppai");
  }

  handlerMakeThread() {}

  render() {
    return (
      <div>
        {nodeId}
        <br />
        {this.state.tokenNum}
        <br />
        <Button
          onClick={() => {
            async function sync(self) {
              await self.props.blockchainApp.mine();
              self.setState({ tokenNum: token.nowAmount() });
            }
            sync(this);
          }}
        >
          mine
        </Button>
        <br />
        target
        <TextField
          onChange={e => this.setState({ targetId: e.target.value })}
        />
        <br />
        amount
        <TextField
          onChange={e => this.setState({ tokenAmount: e.target.value })}
        />
        <br />
        <Button
          onClick={() => {
            this.props.blockchainApp.makeTransaction(
              this.state.targetId,
              this.state.tokenAmount
            );
          }}
        >
          send
        </Button>
        <br />
      </div>
    );
  }
}
