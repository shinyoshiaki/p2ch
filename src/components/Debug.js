import React, { Component } from "react";
import { Button, TextField } from "material-ui";
import * as format from "../constants/format";

export default class Interface extends Component {
  constructor(props) {
    super(props);
    this.state = { title: "", body: "" };
    this.props.connect.ev.on("onCommand", data => {
      this.setState({ msg: data.toString() });
    });
  }

  render() {
    return (
      <div>
        log
        <br />
        <br />
        {this.state.msg}
        <br />
        <br />        
        thread
        <br />
        <TextField onChange={e => this.setState({ title: e.target.value })} />
        <br />
        <TextField onChange={e => this.setState({ body: e.target.value })} />
        <br />
        <Button
          onClick={() => {
            const data = format.thread(this.state.title, this.state.body);
            this.props.connect.send(JSON.stringify(data));
          }}
        >
          send
        </Button>
        <br />
        <Button onClick={() => localStorage.clear()}>clear</Button>
      </div>
    );
  }
}
