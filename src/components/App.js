import React, { Component } from "react";
import { BrowserRouter, Route } from "react-router-dom";
import sha1 from "sha1";

import Home from "./Home";
import Thread from "./Thread";
import BoardIndex from "../app/board/boardIndex";
import Connect from "../kad/connect";
import BlockChainApp from "../blockchain/BlockchainApp";

export default class App extends Component {
  constructor(props) {
    super(props);

    this.user = { id: "" };

    /*let local = JSON.parse(localStorage.getItem('user'))

        if (JSON.stringify(local).length < 10) {
            this.user.id = sha1(Math.random().toString())
            console.log('first use', this.user.id);
            localStorage.setItem('user', JSON.stringify(this.user))
        } else {
            this.user = local
        }

        console.log('login', this.user.id);*/

    this.user.id = sha1(Math.random().toString());
    console.log("user id", this.user.id);

    this.connect = new Connect(this.user.id);
    this.blockchainApp = new BlockChainApp(this.user.id, this.connect);
  }

  render() {
    return (
      <BrowserRouter>
        <div>
          <Route
            exact
            path="/"
            component={() => (
              <Home connect={this.connect} blockchainApp={this.blockchainApp} />
            )}
          />
          <Route
            path="/thread"
            component={() => <Thread connect={this.connect} />}
          />
          <Route
            path="/test"
            component={() => (
              <BoardIndex
                connect={this.connect}
                blockchainApp={this.blockchainApp}
              />
            )}
          />
        </div>
      </BrowserRouter>
    );
  }
}
