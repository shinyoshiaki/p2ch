import React, { Component } from "react";
import AddIcon from "material-ui-icons/AddCircle";
import {
  CircularProgress,
  Tabs,
  Tab,
  Drawer,
  AppBar,
  Button,
  Toolbar,
  Typography,
  IconButton,
  MenuItem
} from "material-ui";
import { Menu } from "material-ui-icons";

import Setting from "./Setting";
import myTheme from "../theme/myThemeFile";
import SimpleTable from "./SimpleTable";
import Debug from "./Debug";
import BlockChainComponent from "./BlockChainComponent";

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = { open: false, index: 0, msg: "", nodeList: "" };
  }

  handleToggle = () => this.setState({ open: !this.state.open });

  handleClose = () => this.setState({ open: false });

  handleChange = (event, index) => {
    this.setState({ index });
  };

  render() {
    return (
      <div>
        <Tabs value={this.state.value} onChange={this.handleChange}>
          <Tab label="blockchain" />
          <Tab label="debug" />
          <Tab label="thread" />
          <Tab label="oppai" />
        </Tabs>
        {this.state.index === 0 && (
          <BlockChainComponent
            connect={this.props.connect}
            blockchainApp={this.props.blockchainApp}
          />
        )}
        {this.state.index === 1 && <Debug connect={this.props.connect} />}
        {this.state.index === 2 && <SimpleTable connect={this.props.connect} />}
        {this.state.index === 3 && <Setting />}
      </div>
    );
  }
}
