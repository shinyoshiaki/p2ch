import React, { Component } from 'react'
import Connect from '../kad/connect.js'
import { List, ListItem, ListItemText, Button, IconButton } from 'material-ui'
import { ArrowBack } from 'material-ui-icons'
import { withRouter } from 'react-router';

class Thread extends Component {
    constructor(props) {
        super(props)
        this.state = { title: this.props.location.state.title, threads: null }
        
        this.props.connect.ev.on('onCommand', (data) => {
            this.setState({ threads: props.connect.threads })
        })
    }

    render() {
        return (
            <div>
                <IconButton onClick={() => this.props.history.push('/')}><ArrowBack /></IconButton>
                {this.state.title}
                <List>
                    {
                        this.state.threads.map((n) => {
                            if (n.title == this.state.title)
                                return (
                                    <ListItem >
                                        <ListItemText primary={n.body} />
                                    </ListItem >
                                )
                        })
                    }
                </List >
            </div>
        )
    }
}

export default withRouter(Thread)